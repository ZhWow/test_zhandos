<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use backend\modules\city\models\Region;
use backend\modules\city\models\Country;

/* @var $this yii\web\View */
/* @var $model backend\modules\city\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin([
        'validationUrl' => Url::toRoute('/city/city/validation-city'),
    ]); ?>

    <?= $form->field($model, 'country_id')->dropDownList(Country::getCountry(), ['prompt' => 'Select country']) ?>

    <?= $form->field($model, 'region_id')->dropDownList(Region::getRegion(), ['prompt' => 'Select region']) ?>

    <?= $form->field($model, 'name', ['enableClientValidation' => false, 'enableAjaxValidation' => true])->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
