<?php

namespace backend\modules\city;

/**
 * Api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\city\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
