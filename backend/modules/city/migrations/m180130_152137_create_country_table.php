<?php

namespace backend\modules\city\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m180130_152137_create_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->unique()->notNull(),
        ], 'ENGINE=InnoDB, COLLATE=utf8_general_ci');

        $this->insert('country', ['name' => 'Казахстан']);
        $this->insert('country', ['name' => 'Китай']);
        $this->insert('country', ['name' => 'Россия']);
        $this->insert('country', ['name' => 'США']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('country');
    }
}
