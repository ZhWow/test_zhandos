<?php

namespace backend\modules\city\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `city`.
 */
class m180130_131233_create_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'region_id' => $this->integer(11)->notNull(),
            'country_id' => $this->integer(11)->notNull(),
        ], 'ENGINE=InnoDB, COLLATE=utf8_general_ci');

        $this->insert('city', ['name' => 'Алматы', 'region_id' => 1, 'country_id' => 1]);
        $this->insert('city', ['name' => 'Астана', 'region_id' => 2, 'country_id' => 1]);
        $this->insert('city', ['name' => 'Тараз', 'region_id' => 3, 'country_id' => 1]);
        $this->insert('city', ['name' => 'Караганда', 'region_id' => 4, 'country_id' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('city');
    }
}
