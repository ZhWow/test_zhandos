<?php

namespace backend\modules\city\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `region`.
 */
class m180130_131627_create_region_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->unique()->notNull(),
        ], 'ENGINE=InnoDB, COLLATE=utf8_general_ci');

        $this->insert('region', ['name' => 'Алматинская']);
        $this->insert('region', ['name' => 'Акмолинская']);
        $this->insert('region', ['name' => 'Жамбылская ']);
        $this->insert('region', ['name' => 'Карагандинская']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('region');
    }
}
