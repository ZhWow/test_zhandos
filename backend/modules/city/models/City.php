<?php

namespace backend\modules\city\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property int $region_id
 */
class City extends ActiveRecord
{

    const INSERT = 'insert';
    const UPDATE = 'update';
    public $newCity;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'region_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'validateName', 'on' => self::INSERT],
            [['country_id', 'region_id'], 'required', 'on' => self::INSERT],
            [['country_id', 'region_id', 'newCity'], 'required', 'on' => self::UPDATE],
            [['name'], 'required', 'on' => self::INSERT, 'message' => 'City cannot be blank.'],
            [['name'], 'required', 'on' => self::UPDATE, 'message' => 'City cannot be blank.'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::UPDATE] = ['name', 'country_id', 'region_id', 'newCity'];
        $scenarios[self::INSERT] = ['name', 'country_id', 'region_id', 'validateName'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'country_id' => 'County ID',
            'region_id' => 'Region ID',
        ];
    }

    public function validateName($attribute, /* @noinspection PhpUnusedParameterInspection */ $params)
    {
        if (!empty($this->country_id) && !empty($this->region_id)) {
            $city = City::find()->where(['name' => $this->$attribute])
                ->andFilterWhere(['country_id' => $this->country_id])
                ->andFilterWhere(['region_id' => $this->region_id])
                ->scalar();
            if ($city) {
                $this->addError($attribute, 'The city already exists');
            }
        }
    }

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }


}
