<?php

namespace backend\modules\api\models;

use Yii;
use yii\base\Model;
use common\models\User;

class Api extends Model
{
    public $userId;
    public $login;
    public $password;
    public $responseType;
    public $method;

    public function rules()
    {
        return [
            [['login', 'password', 'responseType', 'method'], 'trim'],
            [['login', 'password', 'responseType', 'method'], 'string'],
            [['login', 'password', 'method'], 'required'],
            ['login', 'validateLogin'],
            ['login', 'validateLoginStatus'],
            ['login', 'validatePswCount'],
            ['password', 'validatePassword'],
            ['method', 'validateMethod'],
        ];
    }

    public function validateLogin($attribute, /* @noinspection PhpUnusedParameterInspection */
                                  $params)
    {
        $model = User::findOne(['email' => $this->$attribute]);
        if (!$model) {
            $this->addError($attribute, 'Wrong login!');
        } else {
            $this->userId = $model->id;
        }
    }

    public function validateLoginStatus($attribute, /* @noinspection PhpUnusedParameterInspection */
                                        $params)
    {
        $model = User::findOne(['email' => $this->$attribute]);
        if ($model->status === 0) {
            $this->addError($attribute, 'This login ' . $this->$attribute . ' has blocked!');
        }
    }

    public function validatePassword($attribute, /* @noinspection PhpUnusedParameterInspection */
                                     $params)
    {
        $model = User::findOne(['email' => $this->login]);
        if (!empty($model) && !$model->validatePassword($this->$attribute)) {
            if ($this->setPswCount()) $this->addError('password', 'Wrong password!');
        }
    }

    public function validatePswCount()
    {
//        Yii::$app->cache->flush();
        $pswCount = $this->getPswCount();
        if ($pswCount > 8) {
            $status = User::updateAll(['status' => 0], ['email' => $this->login]);
            if ($status) $this->addError('password_count', 'You exceeded the limit! ' . $this->login . ' has been blocked!');
        }
    }

    public function validateMethod($attribute, /* @noinspection PhpUnusedParameterInspection */
                                   $params)
    {
        $requestList = ['insert', 'update', 'select'];
        if (!in_array(strtolower($this->$attribute), $requestList)) {
            $this->addError('request', ' There is no such request => ' . $this->$attribute);
        }
    }

    public function getPswCount()
    {
        return Yii::$app->cache->get('psw_err_' . $this->login);
    }

    public function setPswCount()
    {
        $pswCount = $this->getPswCount() + 1;
        $duration = Yii::$app->params['apiDuration'];
        return Yii::$app->cache->set('psw_err_' . $this->login, $pswCount, $duration);
    }
}