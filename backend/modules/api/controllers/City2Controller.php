<?php
/**
 * Created by PhpStorm.
 * User: Жандос
 * Date: 01.02.2018
 * Time: 0:21
 */

namespace backend\modules\api\controllers;


use Exception;
use Yii;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\Response;
use backend\modules\api\models\Api;
use backend\modules\api\services\InsertCity;

/**
 *  + Полный контроль
 *  + Детальная настройка
 *  - Долгий процесс разработкий
 *  - Парсер данных
 * Class City2Controller
 * @package backend\modules\api\controllers
 */
class City2Controller extends ActiveController
{

    public $modelClass = 'backend\modules\city\models\City';

    public function actionRun()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = Yii::$app->getResponse();

        try {
            // todo have to parse params
            $post = Yii::$app->getRequest()->getBodyParams();;
            $api = new Api();
            $api->load($post, '');

            if ($api->validate()) {
                if (strtolower($api->method) == 'insert') {
                    $city = new InsertCity($post);
                    $response->data = $city->execute();
                    $response->statusText = 'success';
                    $response->setStatusCode(201);
                    $response->getHeaders()->set('Location',  Url::toRoute(['/city/city'], true));
                }
            } else {
                $response->statusText = 'validate error';
                $response->data = $api->getErrors();
                Yii::error($api->getErrors(), 'api');
            }
        } catch (Exception $e) {
            Yii::error($e, 'api');
        }

        return $response;
    }
}