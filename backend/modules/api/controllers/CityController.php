<?php

namespace backend\modules\api\controllers;

use backend\modules\api\services\SelectCity;
use backend\modules\api\services\UpdateCity;
use backend\modules\city\models\City;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;
use Exception;
use backend\modules\api\models\Api;
use backend\modules\api\services\InsertCity;

/**
 * + Быстрая разработка
 * + Парсер данных
 * - Детальная настройка
 * - Ограниченные настройки
 * Class CityController
 * @package backend\modules\api\controllers
 */
class CityController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'list' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'run') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionRun()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [
            'status' => false,
            'message' => 'System error',
        ];

        try {
            $post = Yii::$app->request->post();
            $api = new Api();
            $api->attributes = $post;

            if ($api->validate()) {

                if (strtolower($api->method) == 'insert') {
                    $city = new InsertCity($post);
                } elseif (strtolower($api->method) == 'update') {
                    $city = new UpdateCity($post);
                } else {
                    $city = new SelectCity($post);
                }

                $result = $city->execute();

            } else {
                $result['message'] = $api->getErrors();
                Yii::error($api->getErrors(), 'api');
            }
        } catch (Exception $e) {
            Yii::error($e, 'api');
        }

        return $result;
    }
}