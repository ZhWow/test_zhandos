<?php

namespace backend\modules\api\services;

use backend\modules\city\models\Country;
use backend\modules\city\models\Region;

abstract class BaseCity
{
    public $post;

    public function __construct($post)
    {
        $this->post = $post;
    }

    abstract public function execute();

    public function findCountryId($name)
    {
        return Country::find()->select('id')->andFilterWhere(['name' => $name])->scalar();
    }

    public function findRegionId($name)
    {
        return Region::find()->select('id')->andFilterWhere(['name' => $name])->scalar();
    }
}