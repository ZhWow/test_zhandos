<?php

namespace backend\modules\api\services;

use Yii;
use Exception;
use backend\modules\city\models\City;
use yii\db\ActiveQuery;

class SelectCity extends BaseCity
{
    public function execute()
    {
        $result = [
            'status' => false,
            'message' => 'Error to select',
        ];

        try {
            $city = new City();
            $query = $city->find()
                ->with([
                    'country' => function (ActiveQuery $query) {
                        $query->select('id, name');
                    },
                    'region' => function (ActiveQuery $query) {
                        $query->select('id, name');
                    },
                ]);

            if (!empty($this->post['city'])) $query->andFilterWhere(['name' => $this->post['city']]);
            if (!empty($this->post['country'])) $query->andFilterWhere(['country_id' => $this->findCountryId($this->post['country'])]);
            if (!empty($this->post['region'])) $query->andFilterWhere(['region_id' => $this->findRegionId($this->post['region'])]);

            $response = $query->asArray()->all();
            $result['response'] = $response;
            $result['message'] = 'Success';
            $result['status'] = 'true';

        } catch (Exception $e) {
            Yii::error($e, 'api');
        }

        return $result;
    }
}