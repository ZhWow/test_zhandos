<?php

namespace backend\modules\api\services;

use backend\modules\city\models\City;

class UpdateCity extends BaseCity
{
    public function execute()
    {
        $result = [
            'status' => false,
            'message' => 'Error to update',
        ];

        $city = new City();
        $city->setScenario('update');

        if (!empty($this->post['city'])) $city->name = $this->post['city'];
        if (!empty($this->post['newCity'])) $city->newCity = $this->post['newCity'];
        if (!empty($this->post['country'])) $city->country_id = $this->findCountryId($this->post['country']);
        if (!empty($this->post['region'])) $city->region_id = $this->findRegionId($this->post['region']);

        if ($city->validate()) {
            $query = $city->find()
                ->andFilterWhere(['name' => $city->name])
                ->andFilterWhere(['country_id' => $city->country_id])
                ->andFilterWhere(['region_id' => $city->region_id]);
            $cityResult = $query->one();

            if ($cityResult) {
                $cityResult->name = $this->post['newCity'];
                if ($cityResult->validate() && $cityResult->save()) {
                    $result['status'] = true;
                    $result['message'] = 'Successfully has been updated';
                }
            } else {
                $result['message'] = 'Not found this city ' . $city->name;
            }
        } else {
            $result['message'] = $city->getErrors();
        }

        return $result;
    }
}