<?php

namespace backend\modules\api\services;

use backend\modules\city\models\City;

class InsertCity extends BaseCity
{
    public function execute()
    {
        $result = [
            'status' => false,
            'message' => 'System error',
        ];

        $city = new City();
        $city->setScenario('insert');
        if (!empty($this->post['city'])) $city->name = $this->post['city'];
        if (!empty($this->post['country'])) $city->country_id = $this->findCountryId($this->post['country']);
        if (!empty($this->post['region'])) $city->region_id = $this->findRegionId($this->post['region']);

        if ($city->validate() && $city->save()) {
            $result['status'] = true;
            $result['message'] = 'Successfully has been created';
        } else {
            $result['message'] = $city->getErrors();
        }

        return $result;
    }
}